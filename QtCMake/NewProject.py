#!/usr/bin/python

# No license, use it as you want ! Please share your good ideas to improve :)

# This script creates a Qt/QML project with the name given as parameter.
# It creates a folder, a templated CMakeLists.txt, a simple main.cpp and
# main.qml, a config.h and even an automatic refresh for qml files (simply
# save the file and it will refresh the qml tree).

import sys
import os

if (len(sys.argv) != 2):
    print "usage: " + sys.argv[0] + " ProjectName"
    exit(1)

projectName = str(sys.argv[1])
upperName = projectName.upper()

if (os.path.exists(projectName)):
    print "This project already exists."
    exit(1)

os.makedirs(projectName)

# Generates all sub folders
srcFolder = projectName + "/src"
if not (os.path.exists(srcFolder)):
    os.makedirs(srcFolder)

resourceFolder = projectName + "/resources"
if not (os.path.exists(resourceFolder)):
    os.makedirs(resourceFolder)

buildFolder = projectName + "/build"
if not (os.path.exists(buildFolder)):
    os.makedirs(buildFolder)

cmakeFolder = projectName + "/cmake"
if not (os.path.exists(cmakeFolder)):
    os.makedirs(cmakeFolder)

internalFolder = srcFolder + "/internal"
if not (os.path.exists(internalFolder)):
    os.makedirs(internalFolder)

# Generates CMakeLists.txt

cmakeFileString = "\
cmake_minimum_required(VERSION 3.0.0)\n\
\n\
set(QT5_PLATFORM_SDK /opt/Qt/5.4/gcc_64 CACHE PATH \"Path to Qt5 SDK (/path/to/Qt/<version>/<target>/\")\n\
\n\
# CMake configuration\n\
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)\n\
set(CMAKE_PREFIX_PATH ${QT5_PLATFORM_SDK}/lib/cmake)\n\
set(CMAKE_AUTOMOC ON)\n\
set(CMAKE_AUTORCC ON)\n\
\n\
include(cmake/qt_functions.cmake)\n\
include(cmake/cmake_helpers.cmake)\n\
\n\
project($$PROJECT_NAME)\n\
\n\
# only for qt5_add_resources\n\
find_package(Qt5Core REQUIRED)\n\
\n\
# target configurable variables\n\
set(${PROJECT_NAME}_VERSION_MAJOR 0)\n\
set(${PROJECT_NAME}_VERSION_MINOR 0)\n\
set(${PROJECT_NAME}_VERSION_PATCH 1)\n\
set(${PROJECT_NAME}_VERSION ${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH})\n\
set(${PROJECT_NAME}_COPYRIGHT \"Copyright (c) 2016 Gilles Fernandez\" CACHE STRING \"Copyright\")\n\
set(${PROJECT_NAME}_PROJECT_FOLDER ${CMAKE_CURRENT_SOURCE_DIR})\n\
\n\
# other configurable variables\n\
set(GENERATE_QRC ON CACHE BOOL \"\")\n\
\n\
# adding all dependencies\n\
add_qt_dependency(core)\n\
add_qt_dependency(gui)\n\
add_qt_dependency(quick)\n\
add_qt_dependency(qml)\n\
\n\
include_directories(libs/verdigris/src) # TODO: change?\n\
\n\
# Source files\n\
file(GLOB_RECURSE HEADER_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} src/*.h)\n\
\n\
# Source files\n\
file(GLOB_RECURSE SOURCE_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} src/*.cpp)\n\
\n\
# QML files\n\
file(GLOB_RECURSE QML_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} src/*.qml src/qmldir)\n\
\n\
# Font files\n\
file(GLOB_RECURSE FONTS_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} resources/*.ttf)\n\
\n\
# Generate config.h\n\
configure_file(cmake/config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config.h)\n\
\n\
if (${GENERATE_QRC})\n\
    generate_qrc_file(qml.qrc \"${QML_FILES}\")\n\
    generate_qrc_file(fonts.qrc \"${FONTS_FILES}\")\n\
endif()\n\
\n\
generate_pro_file(\n\
    ${PROJECT_NAME}.pro\n\
    \"app\"\n\
    \"${QT_DEPENDENCIES}\"\n\
    \"${HEADER_FILES}\"\n\
    \"${SOURCE_FILES}\"\n\
    \"${RES_FILES}\"\n\
    \"${EXTERNAL_LIBRARIES}\"\n\
)\n\
\n\
qt5_add_resources(QRC_CPP_FILES ${RES_FILES})\n\
\n\
add_qmake_build(${PROJECT_NAME} \"${EXTERNAL_LIBRARIES}\")\n\
\n\
add_qt_executable(\n\
    ${PROJECT_NAME}\n\
    \"${QT_DEPENDENCIES}\"\n\
    \"${HEADER_FILES}\"\n\
    \"${SOURCE_FILES}\"\n\
    \"${QML_FILES}\"\n\
    \"${QRC_CPP_FILES}\"\n\
    \"${EXTERNAL_LIBRARIES}\"\n\
    \"${FORMULAS_FILES}\"\n\
)\n\
\n\
foreach (LIBRARY ${EXTERNAL_LIBRARIES})\n\
    include_directories(${${LIBRARY}_INCLUDE_DIRS})\n\
    target_link_libraries(${PROJECT_NAME} ${${LIBRARY}_LIBRARIES})\n\
endforeach()\n\
\n\
foreach (EXTERNAL_LIBRARY ${EXTERNAL_LIBRARIES})\n\
    add_dependencies(${PROJECT_NAME} ${EXTERNAL_LIBRARY})\n\
    add_dependencies(qmake-build ${EXTERNAL_LIBRARY})\n\
endforeach()"

cmakeFileString = cmakeFileString.replace("$$PROJECT_NAME", projectName)

cmakeFile = open(projectName + "/CMakeLists.txt", "w")
cmakeFile.write(cmakeFileString)
cmakeFile.close()

# Generates config.h.in that will create config.h containing useful defines
cmakeConfigFileString = "\
#ifndef CONFIG_H\n\
#define CONFIG_H\n\
\n\
#define $$UPPER_NAME_VERSION_MAJOR ${$$PROJECT_NAME_VERSION_MAJOR}\n\
#define $$UPPER_NAME_VERSION_MINOR ${$$PROJECT_NAME_VERSION_MINOR}\n\
#define $$UPPER_NAME_VERSION_PATCH ${$$PROJECT_NAME_VERSION_PATCH}\n\
#define $$UPPER_NAME_VERSION \"${$$PROJECT_NAME_VERSION}\"\n\
#define $$UPPER_NAME_COPYRIGHT \"${$$PROJECT_NAME_COPYRIGHT}\"\n\
#define $$UPPER_NAME_PROJECT_FOLDER \"${$$PROJECT_NAME_PROJECT_FOLDER}\"\n\
\n\
#endif\n"

cmakeConfigFileString = cmakeConfigFileString.replace("$$UPPER_NAME", upperName)
cmakeConfigFileString = cmakeConfigFileString.replace("$$PROJECT_NAME", projectName)

cmakeConfigFile = open(cmakeFolder + "/config.h.cmake", "w")
cmakeConfigFile.write(cmakeConfigFileString)
cmakeConfigFile.close()

# Generates qt_functions.cmake
qtFunctionCmakeFileString = "\
macro(generate_qrc_file FILENAME FILES)\n\
    file(WRITE ${FILENAME} \"<RCC>\\n\")\n\
    file(APPEND ${FILENAME} \"    <qresource prefix=\\\"/\\\">\\n\")\n\
\n\
    foreach(FILE ${FILES})\n\
        file(APPEND ${FILENAME} \"        <file>${FILE}</file>\\n\")\n\
    endforeach(FILE)\n\
\n\
    file(APPEND ${FILENAME} \"    </qresource>\\n\")\n\
    file(APPEND ${FILENAME} \"</RCC>\")\n\
\n\
    list(APPEND RES_FILES ${FILENAME})\n\
endmacro()\n\
\n\
macro(add_qt_dependency DEPENDENCY)\n\
    list(APPEND QT_DEPENDENCIES ${DEPENDENCY})\n\
endmacro()\n\
\n\
macro(generate_pro_file FILENAME TEMPLATE QT_DEPENDENCIES HEADER_FILES SOURCE_FILES RESOURCE_FILES LIBRARIES)\n\
    file(WRITE ${FILENAME} \"TEMPLATE = ${TEMPLATE}\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"QT +=\")\n\
    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"      ${QT_DEPENDENCY}\")\n\
    endforeach(QT_DEPENDENCY)\n\
\n\
    file(APPEND ${FILENAME} \"\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"CONFIG += c++14\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"HEADERS +=\")\n\
    foreach (HEADER_FILE ${HEADER_FILES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"           ${HEADER_FILE}\")\n\
    endforeach(HEADER_FILE)\n\
\n\
    file(APPEND ${FILENAME} \"\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"SOURCES +=\")\n\
    foreach (SOURCE_FILE ${SOURCE_FILES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"           ${SOURCE_FILE}\")\n\
    endforeach(SOURCE_FILE)\n\
\n\
    file(APPEND ${FILENAME} \"\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"RESOURCES +=\")\n\
    foreach (RES_FILE ${RESOURCE_FILES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"             ${RES_FILE}\")\n\
    endforeach(RES_FILE)\n\
\n\
    file(APPEND ${FILENAME} \"\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"INCLUDEPATH += ${CMAKE_CURRENT_BINARY_DIR}\")\n\
    foreach (LIBRARY ${LIBRARIES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"               ${${LIBRARY}_INCLUDE_DIRS}\")\n\
    endforeach(LIBRARY)\n\
\n\
    file(APPEND ${FILENAME} \"\\n\\n\")\n\
\n\
    file(APPEND ${FILENAME} \"LIBS +=\")\n\
    foreach (LIBRARY ${LIBRARIES})\n\
        file(APPEND ${FILENAME} \" \\\\\\n\")\n\
        file(APPEND ${FILENAME} \"        -L${${LIBRARY}_LIBRARY_PATH} -l${LIBRARY}\")\n\
    endforeach(LIBRARY)\n\
endmacro()\n\
\n\
macro(add_qt_executable NAME QT_DEPDENDENCIES HEADER_FILES SOURCE_FILES QML_FILES RESOURCE_FILES LIBRARIES OTHER_FILES)\n\
    include_directories(${CMAKE_CURRENT_BINARY_DIR})\n\
    set(CMAKE_AUTOMOC ON)\n\
    set(CMAKE_AUTORCC ON)\n\
\n\
    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})\n\
        if (${QT_DEPENDENCY} STREQUAL \"core\")\n\
            find_package(Qt5Core REQUIRED)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"gui\")\n\
            find_package(Qt5Gui REQUIRED)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"quick\")\n\
            find_package(Qt5Quick REQUIRED)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"qml\")\n\
            find_package(Qt5Qml REQUIRED)\n\
        else ()\n\
            message(\"${QT_DEPENDENCY} is currently not properly handled by this script\")\n\
        endif()\n\
    endforeach()\n\
\n\
    # WIN32: add resource file\n\
    if (WIN32)\n\
        list(APPEND SRC_FILES src/win32/demo.rc)\n\
    endif()\n\
\n\
    # OS X: generate bundle\n\
    if (APPLE)\n\
        set(MACOSX_BUNDLE_BUNDLE_NAME ${PROJECT_NAME})\n\
        set(MACOSX_BUNDLE_BUNDLE_VERSION ${${NAME}_VERSION})\n\
        set(MACOSX_BUNDLE_COPYRIGHT ${${NAME}_COPYRIGHT})\n\
        set(MACOSX_BUNDLE_GUI_IDENTIFIER \"com.gfz.${NAME}\")\n\
        set(MACOSX_BUNDLE_ICON_FILE ${${NAME}_ICNS_FILENAME_WE})\n\
\n\
        list(APPEND RES_FILES ${${NAME}_ICNS_PATH})\n\
        set_source_files_properties(${${NAME}_ICNS_PATH} PROPERTIES MACOSX_PACKAGE_LOCATION \"Resources\")\n\
        set(MACOSX_PACKAGE_LOCATION \"Resources\")\n\
    endif()\n\
\n\
    add_executable(\n\
        ${NAME}\n\
        WIN32\n\
        MACOSX_BUNDLE\n\
        ${HEADER_FILES}\n\
        ${SOURCE_FILES}\n\
        ${QML_FILES}\n\
        ${RESOURCE_FILES}\n\
        ${OTHER_FILES}\n\
    )\n\
\n\
    # Setup compiler directives\n\
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD 14)\n\
    set_property(TARGET ${NAME} PROPERTY CXX_STANDARD_REQUIRED ON)\n\
\n\
    foreach (QT_DEPENDENCY ${QT_DEPENDENCIES})\n\
        if (${QT_DEPENDENCY} STREQUAL \"core\")\n\
            target_link_libraries(${NAME} Qt5::Core)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"gui\")\n\
            target_link_libraries(${NAME} Qt5::Gui)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"quick\")\n\
            target_link_libraries(${NAME} Qt5::Quick)\n\
        elseif (${QT_DEPENDENCY} STREQUAL \"qml\")\n\
            target_link_libraries(${NAME} Qt5::Qml)\n\
        else ()\n\
            message(\"${QT_DEPENDENCY} is currently not properly handled by this script\")\n\
        endif()\n\
    endforeach()\n\
\n\
    foreach (LIBRARY ${LIBRARIES})\n\
        include_directories(${${LIBRARY}_INCLUDE_DIRS})\n\
        target_link_libraries(${NAME} ${${LIBRARY}_LIBRARIES})\n\
    endforeach()\n\
\n\
    foreach (HEADER_FILE ${HEADER_FILES})\n\
        get_filename_component(HEADER_FILE_NAME ${HEADER_FILE} NAME_WE)\n\
        list(APPEND MOC_FILES \"moc_${HEADER_FILE_NAME}.cpp\")\n\
    endforeach()\n\
\n\
    list(APPEND MOC_FILES \"${PROJECT_NAME}_automoc.cpp\")\n\
\n\
    set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES ${MOC_FILES})\n\
endmacro()\n\
\n\
macro(add_qmake_build NAME LIBRARIES)\n\
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build)\n\
\n\
    set(QMAKEARGS \"\")\n\
    if (CMAKE_BUILD_TYPE STREQUAL \"Debug\")\n\
        string(CONCAT QMAKEARGS ${QMAKEARGS} \"CONFIG+=debug CONFIG+=qml_debug\")\n\
    else ()\n\
        string(CONCAT QMAKEARGS ${QMAKEARGS} \"CONFIG+=release\")\n\
    endif()\n\
\n\
    if (APPLE)\n\
        set(EXPORTARGS \"\")\n\
        foreach(LIBRARY ${LIBRARIES})\n\
                string(CONCAT EXPORTARGS ${EXPORTARGS} ${${LIBRARY}_LIBRARY_PATH}:)\n\
        endforeach()\n\
\n\
        add_custom_target(\n\
            qmake-build\n\
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/\n\
            COMMAND qmake ${QMAKEARGS} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.pro -o ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/Makefile\n\
            COMMAND make\n\
            COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/${NAME}.app/Contents/Frameworks\n\
            COMMAND macdeployqt ${NAME}.app -always-overwrite -qmldir=${CMAKE_CURRENT_SOURCE_DIR}/src\n\
            COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/*/*.dylib ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/${NAME}.app/Contents/Frameworks\n\
        )\n\
    else ()\n\
        add_custom_target(\n\
            qmake-build\n\
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/\n\
            COMMAND qmake ${QMAKEARGS} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.pro -o ${CMAKE_CURRENT_BINARY_DIR}/qmake-build/Makefile\n\
            COMMAND make\n\
        )\n\
    endif()\n\
endmacro()\n"

qtFunctionCmakeFile = open(cmakeFolder + "/qt_functions.cmake", "w")
qtFunctionCmakeFile.write(qtFunctionCmakeFileString)
qtFunctionCmakeFile.close()

# Generates cmake_helpers.cmake
cmakeHelpersFileString = "\
include(ExternalProject)\n\
\n\
macro(add_external_library NAME URL)\n\
        file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build)\n\
\n\
        file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME})\n\
\n\
        if (${URL} MATCHES \".*.git\")\n\
                ExternalProject_Add(\n\
                        ${NAME}\n\
                        GIT_REPOSITORY ${URL}\n\
                        PATCH_COMMAND \"\"\n\
                        INSTALL_COMMAND \"\"\n\
                        UPDATE_COMMAND \"\"\n\
                        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NAME}\n\
                        BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}\n\
                )\n\
        else()\n\
                ExternalProject_Add(\n\
                        ${NAME}\n\
                        URL ${URL}\n\
                        PATCH_COMMAND \"\"\n\
                        INSTALL_COMMAND \"\"\n\
                        UPDATE_COMMAND \"\"\n\
                        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NAME}\n\
                        BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}\n\
                )\n\
        endif()\n\
\n\
        set(${NAME}_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR}/${NAME}/include)\n\
\n\
        set(${NAME}_LIBRARIES ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}/${CMAKE_SHARED_LIBRARY_PREFIX}${NAME}${CMAKE_SHARED_LIBRARY_SUFFIX})\n\
\n\
        set(${NAME}_LIBRARY_PATH ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME})\n\
\n\
        list(APPEND EXTERNAL_LIBRARIES ${NAME})\n\
endmacro()\n"

cmakeHelpersFile = open(cmakeFolder + "/cmake_helpers.cmake", "w")
cmakeHelpersFile.write(cmakeHelpersFileString)
cmakeHelpersFile.close()

# Generates .gitignore file
gitignoreFileString = "\
build\n\
CMakeLists.txt.user\n\
*.qrc\n\
*.pro\n"

gitignoreFile = open(projectName + "/.gitignore", "w")
gitignoreFile.write(gitignoreFileString)
gitignoreFile.close()

# Generates main.cpp
mainCppString = "\
#include <QGuiApplication>\n\
#include <QSurfaceFormat>\n\
#include <QtQml>\n\
\n\
#include <config.h>\n\
\n\
#include \"internal/AutoRefreshView.h\"\n\
\n\
int main(int argc, char *argv[]) {\n\
    QGuiApplication app(argc, argv);\n\
\n\
    AutoRefreshView view;\n\
    view.setTitle(\"$$PROJECT_NAME\");\n\
    view.setMinimumSize(QSize(400, 600));\n\
    view.setResizeMode(QQuickView::SizeRootObjectToView);\n\
\n\
    QSurfaceFormat format = view.format();\n\
    format.setSamples(16);\n\
    view.setFormat(format);\n\
\n\
#ifdef QT_DEBUG\n\
    QString path($$UPPER_NAME_PROJECT_FOLDER);\n\
    path += \"/src/main.qml\";\n\
    view.setSource(QUrl(path));\n\
#else\n\
    view.setSource(QUrl(\"qrc:/src/main.qml\"));\n\
#endif\n\
\n\
    view.show();\n\
\n\
    return app.exec();\n\
}\n"

mainCppString = mainCppString.replace("$$UPPER_NAME", upperName)
mainCppString = mainCppString.replace("$$PROJECT_NAME", projectName)

mainCppFile = open(srcFolder + "/main.cpp", "w")
mainCppFile.write(mainCppString)
mainCppFile.close()

# Generates AutoRefreshView.h
autoRefreshViewHFileString = "\
#ifndef AUTOREFRESHVIEW_H\n\
#define AUTOREFRESHVIEW_H\n\
\n\
#include <QFileSystemWatcher>\n\
#include <QQuickView>\n\
\n\
class AutoRefreshView : public QQuickView {\n\
public:\n\
    explicit AutoRefreshView(QWindow *parent = 0);\n\
    AutoRefreshView(QQmlEngine* engine, QWindow *parent);\n\
    AutoRefreshView(const QUrl &source, QWindow *parent = 0);\n\
\n\
protected:\n\
    QFileSystemWatcher m_watcher;\n\
\n\
    void init();\n\
    void refresh();\n\
\n\
protected slots:\n\
    void handleFileChanged(const QString& file);\n\
};\n\
\n\
#endif\n"

autoRefreshViewHFile = open(internalFolder + "/AutoRefreshView.h", "w")
autoRefreshViewHFile.write(autoRefreshViewHFileString)
autoRefreshViewHFile.close()

# Generates AutoRefreshView.cpp
autoRefreshViewCPPFileString = "\
#include \"AutoRefreshView.h\"\n\
\n\
#include <QQmlEngine>\n\
#include <QDebug>\n\
#include <QDirIterator>\n\
\n\
#include \"config.h\"\n\
\n\
AutoRefreshView::AutoRefreshView(QWindow *parent) : QQuickView(parent) {\n\
    init();\n\
}\n\
\n\
AutoRefreshView::AutoRefreshView(QQmlEngine *engine, QWindow *parent) : QQuickView(engine, parent) {\n\
    init();\n\
}\n\
\n\
AutoRefreshView::AutoRefreshView(const QUrl &source, QWindow *parent) : QQuickView(source, parent) {\n\
    init();\n\
}\n\
\n\
void AutoRefreshView::init() {\n\
#ifdef QT_DEBUG\n\
    connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, &AutoRefreshView::handleFileChanged);\n\
\n\
    QDirIterator it($$UPPER_NAME_PROJECT_FOLDER, QStringList() << \"*.qml\", QDir::Files, QDirIterator::Subdirectories);\n\
\n\
    while (it.hasNext()) {\n\
        it.next();\n\
        m_watcher.addPath(it.filePath());\n\
        qDebug() << \"watching :\" << it.filePath();\n\
    }\n\
#endif\n\
}\n\
\n\
void AutoRefreshView::refresh() {\n\
#ifdef QT_DEBUG\n\
    qDebug() << \"refreshing...\";\n\
    QUrl tmp = source();\n\
    setSource(QUrl());\n\
    engine()->clearComponentCache();\n\
    setSource(tmp);\n\
#endif\n\
}\n\
\n\
void AutoRefreshView::handleFileChanged(const QString &file) {\n\
    Q_UNUSED(file);\n\
    refresh();\n\
}\n"

autoRefreshViewCPPFileString = autoRefreshViewCPPFileString.replace("$$UPPER_NAME", upperName)

autoRefreshViewCPPFile = open(internalFolder + "/AutoRefreshView.cpp", "w")
autoRefreshViewCPPFile.write(autoRefreshViewCPPFileString)
autoRefreshViewCPPFile.close()

# Generates main.qml
mainQmlFileString = "\
import QtQuick 2.3\n\
\n\
Item {\n\
    id: root\n\
\n\
    Text {\n\
        anchors.centerIn: parent\n\
        color: \"red\"\n\
        text: \"$$PROJECT_NAME\"\n\
    }\n\
\n\
}\n"

mainQmlFileString = mainQmlFileString.replace("$$PROJECT_NAME", projectName)

mainQmlFile = open(srcFolder + "/main.qml", "w")
mainQmlFile.write(mainQmlFileString)
mainQmlFile.close();
